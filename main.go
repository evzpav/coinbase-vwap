package main

import (
	vwap "coinbase-vwap/vwapcalculator"
	"flag"
	"io"
	"log"
	"os"
)

const (
	maxDataPoints        int    = 200
	coinbaseWebsocketURL string = "wss://ws-feed.exchange.coinbase.com"
)

func main() {
	fileName := flag.String("file", "", "Specify filename to output the result")
	flag.Parse()

	tradingPairs := []vwap.TradingPair{
		vwap.BTCUSD,
		vwap.ETHUSD,
		vwap.ETHBTC,
	}

	vwapCalc := vwap.NewVWAPCalculator(maxDataPoints, coinbaseWebsocketURL, tradingPairs)

	ws, err := vwapCalc.ConnectToWS()
	if err != nil {
		log.Fatal(err)
	}

	if err := vwapCalc.SubscribeToMatchesChannel(ws); err != nil {
		log.Fatal(err)
	}

	orderChan := make(chan vwap.Order)
	go func() {
		for {
			order, err := vwapCalc.ReadOrder(ws)
			if err != nil {
				log.Println(err)
			}

			orderChan <- *order
		}
	}()

	var output io.Writer
	if fileName != nil && *fileName != "" {
		output, err = os.Create(*fileName)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		output = os.Stdout
	}

	vwapCalc.ConsumeOrders(orderChan, output)

}
