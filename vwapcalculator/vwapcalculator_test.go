package vwapcalculator

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestUpdateVWAPInput(t *testing.T) {
	maxDataPoints := 3

	t.Run("Data points smaller than max", func(t *testing.T) {
		vc := NewVWAPCalculator(maxDataPoints, "", []TradingPair{BTCUSD})
		vwapInput := VWAPInput{
			Orders: []VWAPOrders{
				{
					Size:  5.0,
					Price: 40000.0,
				},
				{
					Size:  10.0,
					Price: 40000.0,
				},
			},
			VWAP: VWAP{
				SumPriceSize: 600000,
				SumSize:      15,
			},
		}

		newOrder := &Order{
			Time:  time.Now(),
			Size:  "25.0",
			Price: "44000.0",
		}

		vc.UpdateVWAPInput(&vwapInput, newOrder)
		assert.Equal(t, 42500.0, vwapInput.VWAP.Calculate())
	})

	t.Run("Data points same as max", func(t *testing.T) {
		vc := NewVWAPCalculator(maxDataPoints, "", []TradingPair{BTCUSD})

		vwapInput := VWAPInput{
			Orders: []VWAPOrders{
				{
					Size:  5.0,
					Price: 40000.0,
				},
				{
					Size:  10.0,
					Price: 40000.0,
				},
				{
					Size:  10.0,
					Price: 40000.0,
				},
			},
			VWAP: VWAP{
				SumPriceSize: 1400000,
				SumSize:      25,
			},
		}

		newOrder := &Order{
			Time:  time.Now(),
			Size:  "20.0",
			Price: "44000.0",
		}

		vc.UpdateVWAPInput(&vwapInput, newOrder)
		assert.Equal(t, 52000.0, vwapInput.VWAP.Calculate())
	})

	t.Run("Invalid price and size - ignore order", func(t *testing.T) {
		vc := NewVWAPCalculator(maxDataPoints, "", []TradingPair{BTCUSD})
		vwapInput := VWAPInput{
			Orders: []VWAPOrders{
				{
					Size:  5.0,
					Price: 40000.0,
				},
				{
					Size:  10.0,
					Price: 40000.0,
				},
			},
			VWAP: VWAP{
				SumPriceSize: 600000,
				SumSize:      15,
			},
		}

		newOrder := &Order{
			Time:  time.Now(),
			Size:  "2;5.0",
			Price: "44~000.0",
		}

		vc.UpdateVWAPInput(&vwapInput, newOrder)
		assert.Equal(t, 40000.0, vwapInput.VWAP.Calculate())
	})

}

func TestConsumeOrders(t *testing.T) {
	maxDataPoints := 200

	t.Run("Data points smaller than max", func(t *testing.T) {
		vc := NewVWAPCalculator(maxDataPoints, "", []TradingPair{BTCUSD})

		orderChan := make(chan Order)

		go func() {
			orders := []Order{
				{
					ProductID: "BTC-USD",
					Size:      "5.0",
					Price:     "45000.0",
				},
				{
					ProductID: "BTC-USD",
					Size:      "10.0",
					Price:     "40000.0",
				},
				{
					ProductID: "BTC-USD",
					Size:      "10.0",
					Price:     "40000.0",
				},
				{
					ProductID: "ETH-USD",
					Size:      "5.0",
					Price:     "3000.0",
				},
				{
					ProductID: "ETH-USD",
					Size:      "10.0",
					Price:     "3500.0",
				},
				{
					ProductID: "ETH-USD",
					Size:      "10.0",
					Price:     "2500.0",
				},
			}

			for _, o := range orders {
				orderChan <- o
			}
			close(orderChan)
		}()

		vc.ConsumeOrders(orderChan, os.Stdout)

		assert.Equal(t, 41000.0, vc.tradingPairToVWAPInput["BTC-USD"].VWAP.Calculate())
		assert.Equal(t, 3000.0, vc.tradingPairToVWAPInput["ETH-USD"].VWAP.Calculate())
	})

}
