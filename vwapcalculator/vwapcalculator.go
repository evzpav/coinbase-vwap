package vwapcalculator

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"time"

	"golang.org/x/net/websocket"
)

type TradingPair string

var (
	BTCUSD TradingPair = "BTC-USD"
	ETHUSD TradingPair = "ETH-USD"
	ETHBTC TradingPair = "ETH-BTC"
)

const (
	MatchesChannel string = "matches"
)

type Subscription struct {
	Type        string        `json:"type"`
	ProductsIDS []TradingPair `json:"product_ids"`
	Channels    []string      `json:"channels"`
}

type Order struct {
	Type      string      `json:"type"`
	TradeID   int         `json:"trade_id"`
	Time      time.Time   `json:"time"`
	ProductID TradingPair `json:"product_id"`
	Size      string      `json:"size"`
	Price     string      `json:"price"`
	Side      string      `json:"side"`
}

type VWAPOrders struct {
	Size  float64 `json:"size"`
	Price float64 `json:"price"`
}

type VWAPInput struct {
	Orders []VWAPOrders
	VWAP   VWAP
}

type VWAP struct {
	SumPriceSize float64
	SumSize      float64
	Time         time.Time
}

func (v VWAP) Calculate() float64 {
	if v.SumSize <= 0 {
		return 0
	}

	return v.SumPriceSize / v.SumSize
}

type vwapCalculator struct {
	maxDataPoints          int
	websocketURL           string
	tradingPairs           []TradingPair
	tradingPairToVWAPInput map[TradingPair]VWAPInput
}

func NewVWAPCalculator(maxDataPoints int, websocketURL string, tradingPairs []TradingPair) vwapCalculator {
	return vwapCalculator{
		maxDataPoints:          maxDataPoints,
		websocketURL:           websocketURL,
		tradingPairs:           tradingPairs,
		tradingPairToVWAPInput: make(map[TradingPair]VWAPInput),
	}
}

func (vc vwapCalculator) ConnectToWS() (*websocket.Conn, error) {
	return websocket.Dial(vc.websocketURL, "", "http://localhost/")
}

func (vc vwapCalculator) SubscribeToMatchesChannel(ws *websocket.Conn) error {
	subscription := Subscription{
		Type:        "subscribe",
		ProductsIDS: vc.tradingPairs,
		Channels:    []string{MatchesChannel},
	}
	bs, err := json.Marshal(subscription)
	if err != nil {
		return err
	}

	if _, err := ws.Write(bs); err != nil {
		return err
	}

	return nil
}

func (vc vwapCalculator) ReadOrder(ws *websocket.Conn) (*Order, error) {
	var msg = make([]byte, 512)
	var n int
	var err error
	if n, err = ws.Read(msg); err != nil {
		return nil, err
	}

	var order Order
	if err := json.Unmarshal(msg[:n], &order); err != nil {
		return nil, err
	}

	return &order, nil
}

func (vc vwapCalculator) ConsumeOrders(orderChan chan Order, writer io.Writer) {
	for o := range orderChan {
		vc.UpdateInputData(&o)
		vc.Print(vc.tradingPairs, writer)
	}
}

func (vc vwapCalculator) UpdateInputData(order *Order) {
	vwapInput := vc.tradingPairToVWAPInput[order.ProductID]
	vc.UpdateVWAPInput(&vwapInput, order)
	vc.tradingPairToVWAPInput[order.ProductID] = vwapInput
}

func (vc vwapCalculator) UpdateVWAPInput(vwapInput *VWAPInput, order *Order) {
	price, err := parseToFloat(order.Price)
	if err != nil {
		return //ignore order
	}

	size, err := parseToFloat(order.Size)
	if err != nil {
		return //ignore order
	}

	vwapInput.Orders = append(vwapInput.Orders, VWAPOrders{Size: size, Price: price})

	if len(vwapInput.Orders) == vc.maxDataPoints+1 {
		oldestData := vwapInput.Orders[0]
		vwapInput.VWAP.SumPriceSize -= oldestData.Price * oldestData.Size
		vwapInput.VWAP.SumSize -= oldestData.Size
		vwapInput.Orders = vwapInput.Orders[1:] //remove first in array
	}

	vwapInput.VWAP.SumPriceSize += price * size
	vwapInput.VWAP.SumSize += size
	vwapInput.VWAP.Time = order.Time
}

func (vc vwapCalculator) Print(tradingPairs []TradingPair, w io.Writer) {
	var strBuffer bytes.Buffer
	
	for _, p := range tradingPairs {
		input := vc.tradingPairToVWAPInput[p]
		msg := fmt.Sprintf("[%s] VWAP(%d)=%.8f - %v\n", p, len(input.Orders), input.VWAP.Calculate(), formatTime(input.VWAP.Time))
		
		strBuffer.WriteString(msg)
	}

	strBuffer.WriteString("\n")

	_, err := fmt.Fprint(w, strBuffer.String())
	if err != nil {
		return
	}

}

func parseToFloat(p string) (float64, error) {
	f, err := strconv.ParseFloat(p, 64)
	if err != nil {
		return 0, err
	}

	return f, nil
}

func formatTime(time time.Time) string {
	return time.Format("2006-01-02 15:04:05")
}
