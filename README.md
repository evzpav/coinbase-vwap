# Coinbase VWAP Calculator

It connects to [Coinbase](https://www.coinbase.com/) websocket [(docs)](https://docs.cloud.coinbase.com/exchange/docs/overview) and subscribes to a channel called `matches` to get the latest match orders(trades) for specific trading pairs.

The default trading pairs are: `BTC-USD`, `ETH-USD` and `ETH-BTC`.

It then calculates the [VWAP](https://en.wikipedia.org/wiki/Volume-weighted_average_price) for the last 200 orders. At the beginning it will calculate the VWAP for less than 200 and it will cap to 200 as soon as they are received through the websocket and print in real time the results for each pair in the terminal.

As soon as the orders are received, they are parsed and are sent to a channel of orders.
This channel than is consumed - each trading pair has it `VWAPInput` struct that holds in memory relevant data to avoid looping through the array of orders each time a new order comes in. The orders are appended to an array of orders for each trading pair. And the total sum of `size` (`SumSize`) and the sum of product of `price` and `size` (`SumPriceSize`) are stored in memory as well. So when a new order comes and the array has 1 item above the limit of 200 orders, it removes from the SumPriceSize and also the SumSize of the oldest data and removes the oldest order from the orders array.

It is assumed that the in the case of parsing price and/or size fails, that order can be ignored for the VWAP calculation.

It does not use any libs besides [github.com/stretchr/testify](https://github.com/stretchr/testify) for unit testing.

## Run project
#### Prerequisite:
- [Golang](http://go.dev/)(>=1.11)
```bash
    go run main.go
    # It will print result to terminal

    # or specify file name to save the result to using --file flag:
    go run main.go --file output.txt

```

It will print in the terminal the results:

```bash
 #[pair] VWAP(lengthOfData)=vwapCalculated - YYYY-MM-DD HH:mm:ss

[BTC-USD] VWAP(200)=42851.61173160 - 2022-01-11 19:42:26
[ETH-USD] VWAP(200)=3236.22169875 - 2022-01-11 19:42:26
[ETH-BTC] VWAP(6)=0.07554639 - 2022-01-11 19:42:14

[BTC-USD] VWAP(200)=42851.93180212 - 2022-01-11 19:42:26
[ETH-USD] VWAP(200)=3236.22169875 - 2022-01-11 19:42:26
[ETH-BTC] VWAP(6)=0.07554639 - 2022-01-11 19:42:14

[BTC-USD] VWAP(200)=42851.93180212 - 2022-01-11 19:42:26
[ETH-USD] VWAP(200)=3236.22195579 - 2022-01-11 19:42:26
[ETH-BTC] VWAP(6)=0.07554639 - 2022-01-11 19:42:14

```

## To run unit tests
```bash
    go test -v vwapcalculator/*.go
```
